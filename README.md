# Paragon Gin

Gin and Layout builder improvements intended to be used on Paragon sites. The goal of these improvements is to make
the authoring experience better.

## Required modules
- Gin
- Gin LB
- Paragon Core

## Recommended Modules
- Layout Builder Browser

## Features
### Layout Builder
- Adds Gin theme setting for toggling between icon + list view of blocks
- UI style improvements for Section Edit links
- Tooltip to show block type when hovering on layout builder
- Various layout builder style improvements
- Refined top menu with "View Edit Layout Builder Version History" shortcuts
- Update titles of inline blocks with the field_heading value if possible

## Known Issues
- Removes the block search bar becuase it doesn't work
